function [h_p,h_p_p]=phistat_der(Tval,phistat_f,T)
h=1e-4;
h_p_p =0;

if T==Tval(1)
    T1=T;      
    hf1=phistat_f(T1);

    T2=T+h;     
    hf2=phistat_f(T2);
    
    h_p=(hf2-hf1)/h;
    
elseif T==Tval(end)
    T1=T-h;   
    hf1=phistat_f(T1);

    T2=T;     
    hf2=phistat_f(T2);
    
    h_p=(hf2-hf1)/h;
else
    
    T1=T-h;
    hf1=phistat_f(T1);

    T2=T+h;     
    hf2=phistat_f(T2);
    
    hf0=phistat_f(T);

    h_p=(hf2-hf1)/(2*h);
    h_p_p=(hf2-2*hf0+hf1)/h^2;
end
    
    