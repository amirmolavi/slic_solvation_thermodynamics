function Predictsolventmodels(index)
global eps_in conv_factor phi_toggle total_solvent_info solvent removed_ion temp FID_PREDICT
temp=25;
TSV(index) = [total_solvent_info(index).Tmin];
TEV(index) = [total_solvent_info(index).Tmax];
TInterval=[TSV(index),TEV(index)];
eps_hat = ((total_solvent_info(index).eps_s(temp))-eps_in)/(total_solvent_info(index).eps_s(temp));
eps_hat_p=((total_solvent_info(index).deps_dt(temp))*(total_solvent_info(index).eps_s(temp))...
   -(total_solvent_info(index).deps_dt(temp))*((total_solvent_info(index).eps_s(temp))-eps_in))/((total_solvent_info(index).eps_s(temp)))^2;

Eps=total_solvent_info(index).eps_s(temp);
dEps_dT=total_solvent_info(index).deps_dt(temp);
deltaT = 1e-4;
d2Eps_dT2 = (total_solvent_info(index).deps_dt(temp+deltaT)- ...
     total_solvent_info(index).deps_dt(temp-deltaT))/(2*deltaT); % for now. 
eps_hat_p_p = (Eps^2*d2Eps_dT2 - 2*dEps_dT^2*Eps +2*(Eps-eps_in)*dEps_dT^2 ...
       - (Eps-eps_in)*d2Eps_dT2*Eps)/Eps^3;

   tol=1e-15; 

    
    TSV = [];
    TEV = [];
    
if phi_toggle==0
    fprintf(FID_PREDICT,'\n \n \n');
    fprintf(FID_PREDICT,['Predicted thermodynamics for ions W/O phi_{static} in ', num2str(solvent(index).Name),'\n']);
    fprintf(FID_PREDICT,'-------------------------------------------------------------------------\n');
    fprintf(FID_PREDICT,'-------------------------------------------------------------------------\n');    
    fprintf(FID_PREDICT,'Ion     Delta G       Delta S       C_p \n');
    fprintf(FID_PREDICT,'-------------------------------------------------------------------------\n');    
elseif phi_toggle==1      
    fprintf(FID_PREDICT,'\n \n \n');
    fprintf(FID_PREDICT,['Predicted thermodynamics for ions W/ phi_{static} in ', num2str(solvent(index).Name),'\n']);
    fprintf(FID_PREDICT,'-------------------------------------------------------------------------\n');
    fprintf(FID_PREDICT,'-------------------------------------------------------------------------\n');    
    fprintf(FID_PREDICT,'Ion     Delta G       Delta S       C_p \n');
    fprintf(FID_PREDICT,'-------------------------------------------------------------------------\n');
end


for j=1:length(removed_ion);
    q=removed_ion(j).q;
    r=removed_ion(j).r;    
    [DG_NLBC_TANH,DS_NLBC_TANH,En,dphidn,Sigma,sigman_nlbc_tanh,Cp_NLBC_TANH]= ...                               %Function for symmetric NLBC TANH
        NLBC_TANH(eps_hat,eps_hat_p,r,tol,q,total_solvent_info(index),temp,eps_in,TInterval,conv_factor,eps_hat_p_p);
    fprintf(FID_PREDICT,'%s  %10.8f %10.8f %10.8f\n',removed_ion(j).Name,DG_NLBC_TANH,DS_NLBC_TANH,Cp_NLBC_TANH);
end