function x=mytanhopt_temp(t,index,x0)
global phi_toggle 
global solvent
options = optimoptions('lsqnonlin');
options = optimoptions(options,'Display', 'off');

%options = optimoptions(@fmincon,'Algorithm','sqp','MaxIterations',1e6);

if phi_toggle == 0
    if strcmp(solvent(index).Name,'PrOH')
        lb = [-10 -100 -10 -10];
        ub = [10 100 10 10];
    else
        lb = [-inf -inf -inf -inf];
        ub = [inf inf inf inf];
    end
    
elseif phi_toggle == 1
    if strcmp(solvent(index).Name,'PrOH')
        lb = [-10 -100 -10 -10 1];
        ub = [10 100 10 10 -1];
    else
        lb = [-inf -inf -inf -inf  -inf];
        ub = [inf inf inf inf inf];
    end
end
    for i = 1:length(t) 
        Ref = Calc_DeltaG_exp(t(i),solvent(index),index);
        error=1e10;
        y=@(x) amsa_temp(x,t(i),Ref,index);
            while error>1e-10            
%                 x(i,:)=fminsearch(y,x0,options);
                  [x(i,:),resnorm,residual,exitflag,output,lambda,jacobian]=lsqnonlin(y,x0,lb,ub,options);
                error=norm(x(i,:)-x0);
                x0=x(i,:);
            end
    fprintf('%5s \n',['Optimized at ', num2str(t(i)) ,' C for solvent: ', num2str(solvent(index).Name)]);
    
%     fileID = fopen('Parameters_At_25_C.txt', 'a');
%     fprintf(fileID,'%6s %18s %12s %12s %12s %12s \r\n','Solvent','Temperature (C)','Alpha','Beta','Gamma','Mu'); 
%     fprintf(fileID,'-------------------------------------------------------------------------------------------\r\n');
%     fprintf(fileID,'%6s %12.2f %20.8f %12.8f %12.8f %12.8f \r\n',num2str(solvent(j).Name),t(i),x(1,1,j),x(1,2,j),x(1,3,j),x(1,4,j));
%     fclose(fileID);
    end
