function a_p=MSAder(R_s,Tval,T,epsin,R,eps_out_f,delta_s,delta_s_der)

h=1e-4;

if T==Tval(1)
    T1=T;       
    epsout = eps_out_f(T1);
    epshat = (epsout-epsin)/epsout;
    delta_s1=delta_s+(T1-25)*delta_s_der;
    Var1=epshat/(R+delta_s1);
    T2=T+h;     
    epsout = eps_out_f(T2);
    epshat = (epsout-epsin)/epsout;
    delta_s1=delta_s+(T2-25)*delta_s_der;
    Var2=epshat/(R+delta_s1);
    a_p=(Var2-Var1)/h;
elseif T==Tval(end)
    T1=T-h;       
    epsout = eps_out_f(T1);
    epshat = (epsout-epsin)/epsout;
    delta_s1=delta_s+(T1-25)*delta_s_der;
    Var1=epshat/(R+delta_s1);
    T2=T;      
    epsout = eps_out_f(T2);
    epshat = (epsout-epsin)/epsout;
    delta_s1=delta_s+(T2-25)*delta_s_der;
    Var2=epshat/(R+delta_s1);
    a_p=(Var2-Var1)/h;
else
    T1=T-h;       
    epsout = eps_out_f(T1);
    epshat = (epsout-epsin)/epsout;
    delta_s1=delta_s+(T1-25)*delta_s_der;
    Var1=epshat/(R+delta_s1);
    T2=T+h;      
    epsout = eps_out_f(T2);
    epshat = (epsout-epsin)/epsout;
    delta_s1=delta_s+(T2-25)*delta_s_der;
    Var2=epshat/(R+delta_s1);
    a_p=(Var2-Var1)/(2*h);
end
    
    
    
    
