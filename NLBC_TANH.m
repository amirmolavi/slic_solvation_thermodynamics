function [DG,DS,En,dphidn,Sigma,sigma_nominal,Cp]= NLBC_TANH(eps_hat,eps_hat_p,r,tol,q,solventinfo,t,eps_in,TInterval,conv_factor,eps_hat_p_p)
    alpha =     solventinfo.alpha_tanh_fit(t);
    beta  =     solventinfo.beta_tanh_fit(t);
    gamma =     solventinfo.gamma_tanh_fit(t);
    mu =        solventinfo.mu_tanh_fit(t);
    phi_stat=   solventinfo.phi_stat_tanh_fit(t);
    
    dGdN = -1./r.^2;
    dphidn=-q./r.^2;
    ERROR=1e10;
    sigma_0=eps_hat*q*dGdN;
    while ERROR>tol
        sigma=sigma_0;
        E_n_p=-q*dGdN-(-0.5)*sigma; 
        hf=alpha*tanh(beta*(E_n_p)-gamma)+mu;
        sigma_0=eps_hat*q*dGdN./(1+hf);
        ERROR1=max(abs((sigma-sigma_0)./sigma_0));
        ERROR2=max(abs(sigma-sigma_0));
        ERROR=max(ERROR1,ERROR2);
    end
    Sigma=sigma_0;   
    En=-q*dGdN-(-0.5)*Sigma;
    hf=alpha*tanh(beta*(En)-gamma)+mu;
    [h_p,h_p_p]=hder_tanh(TInterval,t,eps_in,r,solventinfo.eps_s,q,solventinfo.alpha_tanh_fit,solventinfo.beta_tanh_fit,solventinfo.gamma_tanh_fit,solventinfo.mu_tanh_fit); 
    [phistat_p,phistat_p_p]=phistat_der(TInterval,solventinfo.phi_stat_tanh_fit,t);
    DS=-1000*(0.5*conv_factor*r.*dGdN*q^2.*(eps_hat_p.*(1+hf)-h_p.*eps_hat)./(1+hf).^2+phistat_p*q*conv_factor);
    DG=0.5*conv_factor*r.*Sigma*q+phi_stat*q*conv_factor;
    sigma_nominal=(DG*2)./(conv_factor*r*q);

    one_plus_h = 1+hf;
    uglymess = (eps_hat_p_p.*one_plus_h.^2 - 2 * eps_hat_p*h_p.*one_plus_h ...
		+ 2 * eps_hat * h_p.^2 - eps_hat*h_p_p.*one_plus_h)./one_plus_h.^3;
    Cp= -1000 * conv_factor * (t+273.15) * (0.5*r.*dGdN*q^2 .*uglymess + phistat_p_p*q);

 
