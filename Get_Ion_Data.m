function [Ion_Data,EXP_G_N,EXP_S_N,EXP_HEATCAP_N,removed_ion] = Get_Ion_Data_Amir(solvent)
global Ion_Color ion_ref
% Radius Shannon and Prewitt in Angstroms Fawcett04
Li=struct('Name','Li','r',0.88,'q',1,'color',1 );
Na=struct('Name','Na','r',1.16,'q',1,'color',2 );
K =struct('Name','K' ,'r',1.52,'q',1,'color',3 );
Rb=struct('Name','Rb','r',1.63,'q',1,'color',4 );
Cs=struct('Name','Cs','r',1.84,'q',1,'color',5 );
F =struct('Name','F' ,'r',1.19,'q',-1,'color',6);
Cl=struct('Name','Cl','r',1.67,'q',-1,'color',7);
Br=struct('Name','Br','r',1.82,'q',-1,'color',8);
I =struct('Name','I' ,'r',2.06,'q',-1,'color',9);

ion_ref =[Li,Na,K,Rb,Cs,F,Cl,Br,I]; 


EXP_G = solvent.EXP_G;
EXP_S = solvent.EXP_S;
EXP_HEATCAP = solvent.EXP_HEATCAP;
k=1;
kk=1;
for i=1:length(EXP_G);    
    if EXP_G(i)~=0 && EXP_S(i)~=0 && EXP_HEATCAP(i)~=0
        EXP_G_N(k)=EXP_G(i); 
        EXP_S_N(k)=EXP_S(i); 
        EXP_HEATCAP_N(k)=EXP_HEATCAP(i);
        Ion_Data(k)=ion_ref(i);

        k=k+1;
    else
        removed_ion(kk)=ion_ref(i);
        kk=kk+1;
    end
end

Ion_Color=zeros(1,length(EXP_G_N));
k=1;
kk=1;
for i=1:length(EXP_G); 
    if EXP_G(i)~=0 && EXP_S(i)~=0 && EXP_HEATCAP(i)~=0
        Ion_Color(k)=ion_ref(i).color;
        k=k+1;
    else
        remove_Ion_Color(kk)=ion_ref(i).color;
        kk=kk+1;
    end
end
        
        
