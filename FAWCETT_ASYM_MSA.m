function [DG,DS,dphidn,Sigma]= FAWCETT_ASYM_MSA(eps_hat,r,q,solventinfo,t,eps_in,TInterval,conv_factor)

        if q>0
            ds=solventinfo.msa_param_cat;
            dsder=solventinfo.msa_param_cat_slope;
        else
            ds=solventinfo.msa_param_an;
            dsder=solventinfo.msa_param_an_slope;
        end
        Sigma=-q*eps_hat*(1/(r*(r+ds)));
        dphidn=-q*(1/(r^2)) ;
        DS=1000*0.5*conv_factor*q^2* MSAder(solventinfo.rs,TInterval,t,eps_in,r,solventinfo.eps_s,ds,dsder);
        DG=conv_factor*0.5*q*r*Sigma;

        